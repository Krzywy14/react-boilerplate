const webpack 					= require('webpack');
const autoprefixer 				= require('autoprefixer');
const path 						= require('path');
const WebpackNotifierPlugin 	= require('webpack-notifier');
const MiniCssExtractPlugin 		= require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin 	= require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin 			= require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin 		= require('html-webpack-plugin');
const appVersion 				= require('./package.json').version;

module.exports = (env, argv) => {
	return{
		entry: './src/index.jsx',
		module: {
			rules: [
				{
					test: /\.(js|jsx)$/,
					exclude: /node_modules/,
					use: ['babel-loader']
				},{
					test: /\.(scss|css)$/,
					exclude: [/webfonts/],
					use:[
						argv.mode === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
						{ loader: 'css-loader', options: { sourceMap: true } },
						{ loader: 'postcss-loader', options: { sourceMap: true } },
						{ loader: 'sass-loader', options: { sourceMap: true } },
					]
				}
			]
		},
		plugins: [
			new WebpackNotifierPlugin(),
			new MiniCssExtractPlugin({
	            filename: "assets/styles/styles.css",
	            chunkFilename: "[id].css"
	        }),
			new webpack.LoaderOptionsPlugin({
				options: {
					postcss: [
						autoprefixer()
					]
				}
			}),
			new HtmlWebpackPlugin({
				version: appVersion,
				environment: argv.mode == 'production' ? 'production.min' : 'development',
				filename: 'index.html',
				template: 'src/indexTemplate.html',
				inject: false
			})
		],
		resolve: {
			extensions: ['*', '.js', '.jsx', '.json'],
			alias: {
			    utils: path.resolve(__dirname, 'src/utils'),
				// helpers: path.resolve(__dirname, 'src/utils/helpers'),
				root: path.resolve(__dirname, 'src')
			}
		},
		devtool: 'source-map',
		output: {
			path: path.resolve(__dirname, 'public'),
			filename: 'index.js'
		},
		externals: {
			'react': 		'React',
			'react-dom': 	'ReactDOM',
		},
		optimization: {
			minimize: argv.mode === 'production' ? true : false,
			minimizer: [
				new OptimizeCSSAssetsPlugin({}),
				new UglifyJsPlugin({
					sourceMap: true,
				})
			],
		},
		devServer: {
			host: "0.0.0.0",
			useLocalIp: true,
			contentBase: 'public',
			historyApiFallback: true,
		}
	}
};