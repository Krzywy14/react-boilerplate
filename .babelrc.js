const presets = [
	"@babel/preset-env",
	"@babel/preset-react"
];
const plugins = [
	"@babel/plugin-syntax-dynamic-import",
	"@babel/plugin-proposal-class-properties",
	"@babel/plugin-proposal-object-rest-spread",
	"@babel/plugin-transform-runtime",
];

module.exports = { presets, plugins };