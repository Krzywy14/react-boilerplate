import React 			from 'react';
import { Trans, 
	withTranslation } 	from 'react-i18next';

import './Footer.scss';

class Footer extends React.Component {
	render() {
		return (
			<footer className="footer__wrap">
				<div className="container row">
					Footer
				</div>
			</footer>
		);
	}
}
export default withTranslation()(Footer);