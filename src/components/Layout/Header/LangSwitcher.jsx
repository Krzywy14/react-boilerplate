import AppConfig 			from 'root/AppConfig';
import React 				from 'react';
import { withTranslation }	from 'react-i18next';

import './LangSwitcher.scss';

class LangSwitcher extends React.PureComponent {
	state= {}

	changeLanguageHandler = (lng) => {
		if (this.props.i18n.language != lng) {
			this.props.i18n.changeLanguage(lng)
		}
	}
	render() {
		return (
			<div className="langSwitcher__wrap">
				<div className="langSwitcher__toggler">{this.props.i18n.language}</div>
				<div className="langSwitcher__switch"> 
					{
						AppConfig.langs.map((elem)=>{
							return(
								<div 
									key={elem}
									onClick={() => this.changeLanguageHandler(elem)} 
									className={ this.props.i18n.language == elem ? "langSwitcher__item langSwitcher__item--current" : "langSwitcher__item"}>
									{elem}
								</div>
							)
						})
					}
				</div>
			</div>
		);
	}
}
export default withTranslation()(LangSwitcher);