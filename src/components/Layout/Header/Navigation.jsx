import AppConfig 			from 'root/AppConfig';
import React 				from 'react';
import { connect } 			from 'react-redux';
import { NavLink, Link }	from 'react-router-dom';
import Closer				from '../../Helpers/Closer';
import PropTypes 			from 'prop-types';
import { _getValue }		from 'utils/helpers';

import './Navigation.scss';

class Navigation extends React.PureComponent {
	state = {}
	createNavChilds = elem => {
		if( elem !== undefined && Array.isArray(elem) && elem.length > 0 ){
			return(
				<ul className="navItemChildren__wrap">
					{
						elem.map((child)=>{
							return(
								<li
								 className="navItemChildren__item"
								 key={child["@id"]}>
									<NavLink
									 to={_getValue(child,'url')}
									 exact
									 className="navItemChildren__href"
									 target={_getValue(child,`targetBlank`) ? '_blank' : ''}>
										{ child.title }
									</NavLink>
									{ this.createNavChilds(_getValue(child,'children')) }
								</li>
							)
						})
					}
				</ul>
			)
		}else{
			return false;
		}
	}
	createNav = menuArray => {
		if( menuArray !== undefined && Array.isArray(menuArray) ){
			let navItems = [];
			menuArray.map( (elem,index) =>
				navItems.push(
					<div
					 className={`
					 	navItem__wrap
					 	${ _getValue(elem,'children') ? 'navItem__wrap--has-childs' : ''}
					 	${ _getValue(elem,'featured') ? 'navItem__wrap--featured' : ''}
					 `}
					 key={`navItem_${index}`}>
						{ 
							_getValue(elem,'url')
							?	<NavLink
								 to={elem.url}
								 exact
								 className="navItem__inner"
								 target={_getValue(elem,`targetBlank`) ? '_blank' : ''}>
									{ elem.title }
								</NavLink>
							:	<div className="navItem__inner">
									{ elem.title }
								</div>
						}
						{ this.createNavChilds( _getValue(elem,'children')) }
					</div>
				)
			)
			return navItems;
		}
	}
	render() {
		return (
			<React.Fragment>
				<div className={`navigation__wrap ${this.props.openMenu === true ? 'active' : ''} `}>
					<nav className="navigation__inner">
						{ this.createNav(AppConfig.navigation) }
						<div
						 onClick={this.props.closeHandler}
						 className="navigation__close">
						 	&times;
						</div>
					</nav>
				</div>
				<Closer
					 onClicked={this.props.closeHandler}
					 className={this.props.openMenu ? 'active' : ''}/>
			</React.Fragment>
		);
	}
}

Navigation.propTypes = {
	menu: 			PropTypes.array,
	openMenu: 		PropTypes.any,
	closeHandler: 	PropTypes.func,
};

const mapStateToProps = store => {
	return {};
}
export default connect(
	mapStateToProps,
)(Navigation)
