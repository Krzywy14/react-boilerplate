import React 			from 'react';
import { _getValue }	from 'utils/helpers';
import { Link }			from 'react-router-dom';
import Navigation 		from './Navigation';
import LangSwitcher 	from './LangSwitcher';

import './HeaderDefault.scss';


class HeaderDefault extends React.Component {
	state = {
		stickyHeader: false,
		openMenu: false,
	}
	menuToggleHandler = () =>{
		this.setState( (prevState) => {
			return {
				...this.state,
				openMenu: !this.state.openMenu
			}
		});
	}
	handleScroll = (event) => {
		let scrollTop = _getValue(event.target.scrollingElement,'scrollTop');
		if (scrollTop != false && scrollTop > 50) {
			this.setState( (prevState) => {
				if (prevState.stickyHeader !== true) {	
					return {
						...this.state,
						stickyHeader: true
					}
				}
			});
		}else{
			this.setState( (prevState) => {
				if (prevState.stickyHeader !== false) {	
					return {
						...this.state,
						stickyHeader: false
					}
				}
			});
		}
	}
	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll);
	}
	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
	}
	render() {
		return (
			<header className={`header__wrap ${this.state.stickyHeader == true ? 'sticky' : ''}`}>
				<div className="container header__inner">
					<Link to="/" className="header__logo">
						#LOGO
					</Link>
					<Navigation
					 openMenu={this.state.openMenu}
					 closeHandler={ this.menuToggleHandler }/>
					<div
					 onClick={this.menuToggleHandler}
					 className={`header__hamburger ${_getValue(this.state,'openMenu') ? 'active' : ''}`}>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
					<LangSwitcher />
				</div>
			</header>
		);
	}
}
export default HeaderDefault;