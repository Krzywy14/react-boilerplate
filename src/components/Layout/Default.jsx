import React 			from 'react';
import bodyClasses		from 'utils/bodyClassesHelpers';
import Header 			from './Header/HeaderDefault';
import Footer 			from './Footer/Footer';


import './Default.scss';

class Default extends React.Component {
	componentDidMount() {
		bodyClasses(this.props.bodyClasses)
	}
	render() {
    	return (
    		<React.Fragment>
	    		<Header/>
	    		<main className={`content__wrap ${this.props.className || ''}`}>
		    		<div className="row container">
						{this.props.children}
		    		</div>
	    		</main>
	    		<Footer/>
			</React.Fragment>
		);
    };
};

export default Default;