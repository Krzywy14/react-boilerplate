import React 		from 'react';
import LoaderMini 	from './LoaderMini';


import classes from './Loader.scss';

const Loader = props => 
	<div className="loader__wrap">
		<LoaderMini className={`loader__inner ${props.className}`} />
	</div>


export default Loader;