import React from 'react';

import './Closer.scss'

const Closer = props => 
	<div
	 className={`closer__holder ${props.className}`}
	 onClick={props.onClick}>
	</div>

export default Closer;