import React 			from 'react';
import history 			from '../../history';
import { _getValue }	from '../../utils/helpers';

import './Popup.scss';

class Popup extends React.Component {
	state =  {}
	_handlePopupConfirm = (aditionAction=undefined) =>{
		switch (aditionAction.type) {
			case "goto":
				this.props.onPopupClose();
				history.push(aditionAction.value);
				break;
			case "reload":
				this.props.onPopupClose();
				location.reload();
				break;
			case "close":
				this.props.onPopupClose();
				return aditionAction.value;
				break;
			case "update":
				this.props.onPopupClose();
				return aditionAction.value
				break;
			default: 
				this.props.onPopupClose();
				break;
		}
	}
	render() {
		return (
			<div className="popup__wrap">
				<div className={`popup__inner ${this.props.className}`}>
					<div
					 className="popup__close"
					 onClick={ (event) => this._handlePopupConfirm(this.props.action)}>
						&times;
					</div>
					{
						_getValue(this.props,'html')
						?	<div
							 className="popup__content"
							 dangerouslySetInnerHTML={{ __html: this.props.html }}
							/>
						:	<div className="popup__content">
								{this.props.children }
							</div>
					}
				</div>
			</div>
		);
	}
}

export default Popup;