/**
 *
 * This component load responsive background images
 *
 */

import React 			from 'react';
import { _getValue } 	from 'utils/helpers';

class BackgroundResize extends React.Component {
	state = {
		styles: null,
		imgSize: ''
	}
	checkWidth = () =>{
		this.setState( (prevState) => {
			if(window.innerWidth >= 768){
				// full
				return { 
					...this.state,
					styles: {"backgroundImage": `url(${this.props.image})`},
					imgSize: "full"
				}
			}else{
				// small
				return { 
					...this.state,
					styles: {"backgroundImage": `url(${this.props.imageMobile})`},
					imgSize: 'mobile'
				}
			}
		});
	}
	componentDidMount() {
		this.checkWidth();
		let imgSize = '';
		window.addEventListener("resize", ()=>{
			if(window.innerWidth >= 768){
				imgSize = "full";
			}else{
				imgSize = "mobile";
			}
			if( this.state.imgSize !== imgSize){
				this.checkWidth();
			}
		});

	}
	render() {
		const classes = this.props.classes +' background-resize';
		return (
			<div
			 className={classes}
			 style={this.state.styles}>
			 	{this.props.children}
			</div>
		);
	}
}
export default BackgroundResize;