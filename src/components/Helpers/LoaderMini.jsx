import React 	from 'react';

import './LoaderMini.scss';

const LoaderMini = props => 
	<div className="loaderMini__wrap">
		<div className={`loaderMini__inner ${props.className}`}>
			<div className="loaderMini__content">
			</div>
		</div>
	</div>

export default LoaderMini;