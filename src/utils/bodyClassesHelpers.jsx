import { detect }	from "detect-browser";

export const _getBrowserName = () =>{
	const browser = detect();
	if(browser){
		return browser.name;
	}else{
		return '';
	}
}
export const _setBodyClasses = (bodyClasses=[]) => {
	if( Array.isArray(bodyClasses) ){
		document.querySelector("body").className = '';
		bodyClasses.push(_getBrowserName());
		if(bodyClasses !== undefined){
			document.querySelector("body").classList.add(...bodyClasses);
		}
	}else{
		console.error("_setBodyClasses / bodyClasses = Parameter is not an array !");
	}
}

export const bodyClassesHelpers = {
	_getBrowserName,
	_setBodyClasses
}
export default _setBodyClasses;