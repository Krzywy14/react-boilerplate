import {
	Trans,
	withTranslation}	from 'react-i18next';
import { toast } 		from "react-toastify";
import { _getValue }	from 'utils/helpers';
import history 			from 'root/history';
// import * as Sentry 		from '@sentry/browser';

const errorCodes={
	400: <Trans i18nKey="error.api.bed-request" />,
	401: <Trans i18nKey="error.api.unauthorized" />,
	403: <Trans i18nKey="error.api.forbidden" />,
	404: <Trans i18nKey="error.api.not-found" />,
	406: <Trans i18nKey="error.api.unexpected-error-occurred" />,
	408: <Trans i18nKey="error.api.unexpected-error-occurred" />,
	500: <Trans i18nKey="error.api.unexpected-error-occurred" />,
}

const _handleToastClose = (action=undefined) =>{
	if(_getValue(action,'type')){
		switch (action.type) {
			case "goto":
				history.push(action.value);
				break;
			case "reload":
				location.reload();
				break;
			case "close":
				return true;
				break;
			default: 
				return true;
				break;
		}
	}else{
		return true;
	}
}
const getToast = (message, toastType='', callback = {}) => {
	if(_getValue(message,'Trans')){
		message = <Trans i18nKey={message.Trans} />
	}
	switch (toastType) {
		case "error":
			return toast.error(message,{ onOpen: () => _handleToastClose(callback) })
			break;
		case "warning":
			return toast.warning(message,{ onOpen: () => _handleToastClose(callback) })
			break;
		case "info":
			return toast.info(message,{ onOpen: () => _handleToastClose(callback) })
			break;
		case "success":
			return toast.success(message,{ onOpen: () => _handleToastClose(callback) })
			break;
		default: 
			return toast(message,{ onOpen: () => _handleToastClose(callback) })
	}

}
export const responseReconize = (response,toastType='',callback=null) =>{
	if(location.host == '192.168.1.37:8080'){
		console.log(response || '',{
			error:response.error,
			config:response.config,
			code:response.code,
			request:response.request,
			response:response.response
		});
	}
	if( _getValue(response,'code') == 'ECONNABORTED'){
		getToast(<Trans i18nKey="error.api.timeout" />,'error')

	}else if( _getValue(response,'response.status') && errorCodes[response.response.status] ){
		getToast(errorCodes[response.response.status],toastType,callback)

	}else{
		if(toastType == "error"){
			getToast(<Trans i18nKey="error.api.unexpected-error-occurred" />,toastType,callback)
		}else{
			getToast(response,toastType,callback)
		}
	}
	
	// sentry.io error raporting
	if( toastType == "error" ){
		if( _getValue(response,'code') == 'ECONNABORTED' ){
			Sentry.withScope(scope => {
				Sentry.captureException("api timeout")
			});
		}else{
			Sentry.withScope(scope => {
				if(_getValue(response,'response') ){
					Sentry.captureException(response.response)
				}else{
					Sentry.captureException(response)
				}
			});
		}
	}
}