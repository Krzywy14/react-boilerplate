import * as Sentry		from '@sentry/browser';

const getEnenvironment = () =>{
	switch(location.host){
		case 'mydomain.com': 
			return 'staging'
		default:
			return 'develop'
	}
}
if( getEnenvironment() != 'develop'){
	Sentry.init({
		dsn: "sentry.io key",
		environment: getEnenvironment(),
		beforeSend(event,hint) {
			const error = hint.originalException;
			if (error && error.message){
				return event;
		    }
		}
	});
}

export default Sentry;
