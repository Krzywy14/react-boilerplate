export const _getValue = (obj, props) =>{
	return props.split('.').reduce( (acc, p) => {
		if(acc == null || acc == undefined || acc[p] == null || acc[p] == undefined || acc[p] == 'null' || acc[p] == 'undefined'){
			return false;
		}else if( typeof acc[p] === 'object' && Object.keys(acc[p]).length === 0){
			return false;
		}else if( typeof acc[p] === 'array' && acc[p].length == 0 ){
			return false;
		}else{
			return acc[p]
		}
	},obj)

};
export const groupBy = (xs, key) => {
	return xs.reduce( (rv, x) => {
		(rv[x[key]] = rv[x[key]] || []).push(x);
		return rv;
	}, {});
};
export const stringToObject = (string=null, spliter=';') => {
	return string
		.slice(1)
		.split(spliter)
		.map(p => p.split('='))
		.reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {});
};
export const _getResizedImg = (url=null,size=null) =>{
	if(url != null && size !=null){
		let newUrl = [url.substr(0,url.lastIndexOf("/")+1),url.substr(url.lastIndexOf("/")+1)]
		return `${newUrl[0]}${size}/${newUrl[1]}`
	}
};
export const arrToObj = (array,key) =>{
	if(array !== null && array !== undefined && key !== undefined){
		let results = {};
		for (let i = 0; i < array.length; i++) {
			results[array[i][key]] = array[i]
		}
		return results;
	}
}

export default{
	_getValue,
	groupBy,
	stringToObject,
	arrToObj,
};