import AppConfig 		from 'root/AppConfig.jsx';
import axios 			from 'axios';

let instance = axios.create();

instance.defaults.baseURL = AppConfig.API_URL;
instance.defaults.timeout = 60000;

instance.interceptors.request.use(
	request => request,
	error => {
		return Promise.reject(error)
	}
);

instance.interceptors.response.use(
	async response => Promise.resolve(response),
	async error => Promise.reject(error),
);
export default instance;