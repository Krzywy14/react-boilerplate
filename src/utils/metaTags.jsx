import { _getValue }	from "./helpers";

export const metaTags = (metaTags={}) => {
	if( _getValue(metaTags,'title') ){
		document.title = metaTags.title
	}else{
		document.title = `APP Title`
	}

	if( _getValue(metaTags,'keywords') ){
		document.querySelector('meta[name=keywords]').setAttribute("content", metaTags.keywords);
	}

	if( _getValue(metaTags,'description') ){
		document.querySelector('meta[name="description"]').setAttribute("content", metaTags.description);
	}
	if( _getValue(metaTags,'og') ){
		for (let tag in metaTags.og) {
			document.querySelector(`meta[property="og:${tag}"]`).setAttribute("content", metaTags.og[tag]);
		}
	}

}
export default metaTags;