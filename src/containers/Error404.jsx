import React 			from 'react';
import { Link }			from 'react-router-dom';
import { Trans, 
	withTranslation } 	from 'react-i18next';
import Layout 			from '../components/Layout/Default';

import './Error404.scss';

class Error404 extends React.Component {
	render() {
    	return (
    		<Layout 
    		 className="Error404__wrap"
    		 bodyClasses={["Error404"]}>
				<h1 className="Error404__title">
					<Trans i18nKey="error404.title" />
				</h1>
				<div className="Error404__message">
					<Trans i18nKey="error404.message" />
				</div>
				<Link className="Error404__button button button--alt button--round" to="/">
					<Trans i18nKey="error404.button" />
				</Link>
    		</Layout>
		);
    };
};

export default withTranslation()(Error404)
