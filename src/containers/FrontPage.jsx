import React 			from 'react';
import { Trans, 
	withTranslation } 	from 'react-i18next';

import Layout 			from '../components/Layout/Default';

import './FrontPage.scss';

class FrontPage extends React.Component {
	render() {
		return (
				<Layout 
				 className="FrontPage"
				 bodyClasses={["FrontPage"]}>
				 	<Trans i18nKey="hello_word" />
				</Layout>
		);
	};
};

export default withTranslation()(FrontPage);