import AppConfig 				from './AppConfig';
import i18n 					from "i18next";
import { initReactI18next } 	from "react-i18next";
import LanguageDetector 		from 'i18next-browser-languagedetector';
import Backend 					from 'i18next-xhr-backend';


i18n
	.use(Backend)
	.use(LanguageDetector)
	.use(initReactI18next)
	.init({
		lng: "pl",
		debug: false,
		keySeparator: '.',
		fallbackLng: AppConfig.langs,
		backend:{
			loadPath: '/locales/{{lng}}.json',
		},
		interpolation: {
			escapeValue: false
		},
		detection:{
			order: ['localStorage', 'navigator', 'htmlTag'],
			lookupLocalStorage: 'i18nextLng',
			htmlTag: document.documentElement,
			caches: ['localStorage']
		},
		returnEmptyString: false,
		react: {
			wait: true,
			useSuspense: false,
		}
	});
export default i18n;