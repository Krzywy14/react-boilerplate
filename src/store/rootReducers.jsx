import * as actionTypes from './rootActions';
import history 			from '../history';
import { _getValue }	from 'utils/helpers';

const initialState = {
	isFething: false,
}

const reducer = (state = initialState, action) => {
	switch(action.type) {
		case actionTypes.FETHING_DATA:
			return{
				...state,
				isFething: action.payload
			}
			break;
	}
	return state;
}

export default reducer;