import thunk            from 'redux-thunk'
import { createStore,
	applyMiddleware }   from 'redux';
import { _getValue }	from 'utils/helpers';
import reducer 			from './reducer';
import { compose } 		from 'redux';

const composeEnhancers =
	typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ 
	? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
	: compose;

const enhancer = composeEnhancers(
	applyMiddleware(thunk),
);
const store = createStore(reducer, enhancer);


store.subscribe(() =>{});

export default store;