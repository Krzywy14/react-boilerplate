import API					from 'utils/API';
import { responseReconize }	from 'utils/responseReconize';
import history 				from '../history';
import { _getValue }		from 'utils/helpers';

export const FETHING_DATA 	= 'FETHING_DATA';
export const setFething = fetchingState => {
	return dispatch => {
		dispatch({
			type: FETHING_DATA,
			payload: fetchingState,
		})
	}
}