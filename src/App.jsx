import React 				from 'react';
import { connect } 			from 'react-redux';
import * as rootActions 	from './store/rootActions';
import { _getValue }		from 'utils/helpers';
import { Router, Route, Switch}
							from 'react-router-dom';
import history 				from './history';
import { ToastContainer }	from "react-toastify";

import FrontPage 			from './containers/FrontPage';
import Loader 				from './components/Helpers/Loader';
import Error404 			from './containers/Error404';

import './style.scss'

class App extends React.Component {
	state = {}
	componentDidMount() {
		if( document.querySelector("#loader") ){
			setTimeout(()=>{
				document.querySelector("#loader").removeAttribute("class");
				document.querySelector("#loader").style.opacity = "0";
				setTimeout(()=>{
					if( document.querySelector("#loader") )
						document.querySelector("#loader").remove();
				},500)
			},500)
		}
	}
	scrollToTopWhenRouting = () =>{
		history.listen((location, action) => {
			window.scrollTo(0, 0);
		});
	}
	render() {
		this.scrollToTopWhenRouting();
		return (
			<React.Fragment>
				<Router history={history}>
					<Switch>
						<Route path="/" component={FrontPage} exact/>
						<Route component={ Error404 }/>
					</Switch>
				</Router>
				{_getValue(this.props,'FETHING_DATA')?
					<Loader />
				:''}
				<ToastContainer autoClose={5000} />
			</React.Fragment>
		);
	};
};

const mapStateToProps = store => {
	return {
		FETHING_DATA: store.rootReducers.FETHING_DATA
	};
}
const mapDispatchToProps = dispatch => {
	return {};
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App)