# React Redux APP - WEBPACK

## To use this template required, global instaled
- [node.js](https://nodejs.org/en/)
- [SCSS](http://sass-lang.com)
- [Yarn](https://yarnpkg.com/en/)


## Geting start

1. Clone repository
```bash
git clone git@bitbucket.org:raczejinaczej/react-boilerplate.git
```
2. Get inside react-boilerplate directory
```bash
cd react-boilerplate
```	
5. Install required modules by package manager. Recomend [yarn](https://yarnpkg.com/en/)
```bash
yarn install
```
## Run webpack project 
```bash
yarn start 
```
## Build deploy project by webpack
```bash
yarn build
```
* * *

## Include
- [Normalize.scss](http://nicolasgallagher.com/about-normalize-css/)
- [Bootstrap v4](https://getbootstrap.com/docs/4.0/)
- [React]
- [React DOM]
- [Redux]
- [React props-types]
* * *

### Translate project suported by gulp 
```bash
yarn trans
```
#### Changelog