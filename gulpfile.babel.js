'use strict'

import gulp			from 'gulp';
import sort 		from 'gulp-sort';
import scanner 		from 'i18next-scanner';

gulp.task('i18n',
	() => {
		return gulp.src("./src/**/*.{js,jsx}")
			.pipe(sort())
			.pipe(scanner({
				removeUnusedKeys: false,
				debug: true,
				lngs: ["pl","en","de"],
				defaultValue: "__STRING_NOT_TRANSLATED__",
				func: {
					list: ['t', 'props.t'],
					extensions: ['.jsx']
				},
				trans: {
					component: 'Trans',
					extensions: ['.jsx'],
				},
				resource: {
					// the source path is relative to current working directory
					loadPath: './public/locales/{{lng}}.json',
					
					// the destination path is relative to your `gulp.dest()` path
					savePath: '{{lng}}.json'
				},
				keySeparator: '.'
			}))
			.pipe(gulp.dest("./public/locales/"));
	}
);

gulp.task("default", gulp.series(['i18n'], (done)=>{
	done();
}))